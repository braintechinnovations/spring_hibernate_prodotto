DROP DATABASE IF EXISTS negozio_spring;
CREATE DATABASE negozio_spring;
USE negozio_spring;

CREATE TABLE prodotto(
	prodottoID INTEGER NOT NULL AUTO_INCREMENT,
    codice VARCHAR(10) NOT NULL UNIQUE,
    nome VARCHAR(250) NOT NULL,
    descrizione VARCHAR(250) NOT NULL,
    PRIMARY KEY(prodottoID)
);

SELECT * FROM prodotto;