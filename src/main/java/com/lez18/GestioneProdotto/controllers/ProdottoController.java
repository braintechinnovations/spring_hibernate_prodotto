package com.lez18.GestioneProdotto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez18.GestioneProdotto.models.Prodotto;
import com.lez18.GestioneProdotto.services.ProdottoService;

@RestController
@RequestMapping("/prodotto")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProdottoController {
	
	@Autowired
	private ProdottoService service;

	@GetMapping("/test")
	public String test() {
		return "Hello World!";
	}
	
	@PostMapping("/insert")
	public Prodotto aggiungi_prodotto(@RequestBody Prodotto varProd) {
		return service.saveProdotto(varProd);
	}
	
	@GetMapping("/{prodotto_id}")
	public Prodotto dettaglio_prodotto(@PathVariable int prodotto_id) {
		return service.findById(prodotto_id);
	}
	
	@GetMapping("/")
	public List<Prodotto> cerca_prodotti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{prodotto_id}")
	public boolean elimina_prodotto(@PathVariable int prodotto_id) {
		return service.delete(prodotto_id);
	}
	
	@PutMapping("/update")
	public boolean modifica_prodotto(@RequestBody Prodotto varProd) {
		return service.update(varProd);
	}
}
