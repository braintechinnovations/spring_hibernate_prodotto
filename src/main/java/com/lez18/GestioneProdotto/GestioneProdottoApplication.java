package com.lez18.GestioneProdotto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestioneProdottoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestioneProdottoApplication.class, args);
	}

}
