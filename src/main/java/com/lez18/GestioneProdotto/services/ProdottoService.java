package com.lez18.GestioneProdotto.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lez18.GestioneProdotto.models.Prodotto;

@Service
public class ProdottoService {

	@Autowired
	private EntityManager entMan;						//Equivalente della Session Factoryh
	
	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}
	
	/**
	 * Inserimento di un prodotto
	 * @param objProd	Prodotto proveniente dal controller (dopo il mapping della Request Body)
	 * @return	Oggetto (null se non trovato) con il campo ID riempito!
	 */
	public Prodotto saveProdotto(Prodotto objProd) {
		
		Prodotto temp = new Prodotto();					//Creo un oggetto NUOVO per motivi di sicurezza che clona il precedente!
		temp.setNome(objProd.getNome());
		temp.setCodice(objProd.getCodice());
		temp.setDescrizione(objProd.getDescrizione());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	
		return temp;
		
	}
	
	/**
	 * Restituisce il prodotto a partire dal suo ID autoincrementale
	 * @param varId	Sotto forma di intero, riferimento al database "prodottoID"
	 * @return	Oggetto corrispondente | null
	 */
	public Prodotto findById(int varId) {
		
		Session sessione = getSessione();
		return (Prodotto) sessione
								.createCriteria(Prodotto.class)
								.add(Restrictions.eqOrIsNull("id", varId))		//aLa property è di Hibernate e non del Database
								.uniqueResult();		
	}

	/**
	 * Restituisce tutte le entry (rows) presenti sul database corrispondenti a Prodotto.class
	 * @return	List<Prodotto>
	 */
	public List<Prodotto> findAll(){
		
		Session sessione = getSessione();
		return sessione.createCriteria(Prodotto.class).list();
		
	}
	
	/**
	 * 1. Carico un oggetto tramite il suo ID
	 * 2. Lo marco come da eliminare
	 * 3. Effettuo la Flush per "committare" la modifica
	 * Attenzione al Transactional perché mi permette di mettere i punti 1,2,3 nella stessa transazione!
	 * In questo modo evitiamo che qualcun altro si infili tra la delete a la flush (altre operazioni dal web)
	 * @param varId
	 * @return
	 */
	@Transactional
	public boolean delete(int varId) {
		
		Session sessione = getSessione();
		
		try {
			
			Prodotto temp = sessione.load(Prodotto.class, varId);			//Va in exception se non trova l'oggetto sul DB!
		
			sessione.delete(temp);											//Ha marcato l'oggetto come DA ELIMINARE!
			sessione.flush();
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
		
	}
	
	@Transactional
	public boolean update(Prodotto objProd) {

		Session sessione = getSessione();
		
		try {
			Prodotto temp = sessione.load(Prodotto.class, objProd.getId());		//Cerco il prodotto vecchio 
			
			if(temp != null) {
				temp.setNome(objProd.getNome());
				temp.setCodice(objProd.getCodice());
				temp.setDescrizione(objProd.getDescrizione());
				
				sessione.update(temp);
				sessione.flush();
				
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
}














