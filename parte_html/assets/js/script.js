function inserisciProdotto(){

    var varNome = document.getElementById("varNome").value;
    var varCodice = document.getElementById("varCodice").value;
    var varDescrizione = document.getElementById("varDescrizione").value;

    // document.getElementById("varNome").value = "";
    // document.getElementById("varCodice").value = "";
    // document.getElementById("varDescrizione").value = "";

    var objProdotto = {
        nome: varNome,
        codice: varCodice,
        descrizione: varDescrizione
    };

    //console.log(objProdotto);
    //console.log(JSON.stringify(objProdotto));

    $.ajax(
        {
            url: "http://localhost:8082/prodotto/insert",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(objProdotto),
            dataType: "json",
            success: function(risultato){
                alert("Inserimento effettuato con successo!")
                aggiornaElenco();
            },
            error: function(errore){
                alert("Errore di esecuzione")
                console.log(errore);
            }
        }
    )
}

function stampaProdotti(arrayDaStampare){
    var stringaHtml = "";

    for(var i=0; i<arrayDaStampare.length; i++){
        var temp = arrayDaStampare[i];

        stringaHtml += "<tr>";
        stringaHtml +=      "<td>" + temp.nome + "</td>";
        stringaHtml +=      "<td>" + temp.codice + "</td>";
        stringaHtml +=      "<td>" + temp.descrizione + "</td>";
        stringaHtml +=      "<td>";
        stringaHtml +=      "   <button type=\"button\" class=\"btn btn-danger\" onclick=\"eliminaProdotto(" + temp.id + ")\">";
        stringaHtml +=      "       <i class=\"fas fa-trash-alt\"></i>";
        stringaHtml +=      "   </button>";
        stringaHtml +=      "   <button type=\"button\" class=\"btn btn-warning\" onclick=\"modificaProdotto(" + temp.id + ")\">";
        stringaHtml +=      "       <i class=\"fas fa-edit\"></i>";
        stringaHtml +=      "   </button>";
        stringaHtml +=      "</td>";
        stringaHtml += "</tr>";
    }

    document.getElementById("contenuto-tabella").innerHTML = stringaHtml;
}

function eliminaProdotto(varIdentificatore){

    //Il BackTick si fa con Alt + 96 della tastiera numerica!

    $.ajax(
        {
            //url: "http://localhost:8082/prodotto/" + varIdentificatore,
            url: `http://localhost:8082/prodotto/${varIdentificatore}`,
            type: "DELETE",
            success: function(risultato){
                alert("Eliminazione effettuata con success");
                aggiornaElenco();
            },
            error: function(errore){
                alert("Ops... qualcosa è andato storto ;(");
                console.log(errore);
            }
        }
    )

}

function modificaProdotto(varIdentificatore){
    for(var i=0; i<elencoProdotti.length; i++){
        var temp = elencoProdotti[i];

        if(temp.id == varIdentificatore){
            document.getElementById("varModId").value = temp.id;
            document.getElementById("varModNome").value = temp.nome;
            document.getElementById("varModCodice").value = temp.codice;
            document.getElementById("varModDescrizione").value = temp.descrizione;
        }
    }

    $("#modaleModifica").modal('show');
}

function effettuaModifica(){
    var varModId = document.getElementById("varModId").value;
    var varModNome = document.getElementById("varModNome").value;
    var varModCodice = document.getElementById("varModCodice").value;
    var varModDescrizione = document.getElementById("varModDescrizione").value;

    var objProdotto = {
        id: varModId,
        nome: varModNome,
        codice: varModCodice,
        descrizione: varModDescrizione
    }

    $.ajax(
        {
            url: "http://localhost:8082/prodotto/update",
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(objProdotto),
            dataType: 'json',
            success: function(risultato){
                alert("Modifica effettuata con successo");
                aggiornaElenco();

                document.getElementById("varModId").value = "";
                document.getElementById("varModNome").value = "";
                document.getElementById("varModCodice").value = "";
                document.getElementById("varModDescrizione").value = "";
            
                $("#modaleModifica").modal('hide');
            },
            error: function(errore){
                alert("Ops... non ho modificato nulla ;(");
            }
        }
    )
}

function aggiornaElenco(){
    $.ajax(
        {
            url: "http://localhost:8082/prodotto/",
            type: "GET",
            success: function(risultato){
                elencoProdotti = risultato;
                stampaProdotti(elencoProdotti);
            },
            error: function(errore){
                console.log(errore)
            }
        }
    )
}

var contatore = 0;
var elencoProdotti = [];

aggiornaElenco();
